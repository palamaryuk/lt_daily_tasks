<?php

namespace app\modules\api\controllers;

use app\models\User;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;

class AuthorizationController extends Controller
{
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['index'],
                    'verbs' => ['POST'],
                ]
            ],
            'denyCallback' => function ($rule, \yii\base\InlineAction $action) {
                if ($action->id == 'index') {
                    throw new BadRequestHttpException('Only POST request method is allowed');
                }
            }
        ];

        return $behaviors;
    }

    /**
     * @throws BadRequestHttpException|Exception
     */
    public function actionIndex()
    {
        $bodyParams = Yii::$app->request->bodyParams;


        $user = User::findByUsername($bodyParams['login']);

        if (!$user) {
            throw new BadRequestHttpException("Login not found");
        }

        if (!$user->validatePassword($bodyParams['password'])) {
            throw new BadRequestHttpException("Invalid password");
        }

        if (!$user->getAccessToken()) {
            $user->setAccessToken();
            $user->save();
        }

        return [
            'access_token' => $user->getAccessToken(),
            'expired_at' => $user->access_token_expired_at
        ];
    }
}
