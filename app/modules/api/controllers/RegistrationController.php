<?php

namespace app\modules\api\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;

class RegistrationController extends Controller
{
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['index'],
                    'verbs' => ['POST'],
                ]
            ],
            'denyCallback' => function ($rule, \yii\base\InlineAction $action) {
                if ($action->id == 'index') {
                    throw new BadRequestHttpException('Only POST request method is allowed');
                }
            }
        ];

        return $behaviors;
    }

    /**
     * @throws BadRequestHttpException
     */
    public function actionIndex()
    {
        $bodyParams = Yii::$app->request->bodyParams;


        $user = new User();
        $user->login = $bodyParams['login'];
        $user->setPassword($bodyParams['password']);

        if (!$user->validate()) {
            throw new BadRequestHttpException(Json::encode($user->errors));
        }

        if (!$user->save()) {
            throw new BadRequestHttpException(Json::encode($user->errors));
        }

        return $user->id;
    }
}
