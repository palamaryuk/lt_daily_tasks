<?php

namespace app\modules\api\controllers;

use app\models\UserTasks;
use Yii;
use yii\db\ActiveRecord;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;

class TaskController extends Controller
{
    /**
     * User tasks for the day
     *
     * @return array
     */
    public function actionIndex(): array
    {
        $tasks = UserTasks::find()
            ->alias('ut')
            ->select('
                ut.date_create,
                task.id,
                task.category_id,
                task_categories.name as category_name,
                ut.completed,
                task.name,
                task.description,
                ')
            ->joinWith('task.category')
            ->where([
                'and',
                ['=', 'user_id', Yii::$app->user->id],
                ['>=', 'date_create', gmdate('Y-m-d 00:00:00')],
                ['=', 'skip', 0]
            ])
            ->orderBy('date_create DESC')
            ->asArray()
            ->limit(UserTasks::DAILY_TASKS_COUNT)
            ->all();

        foreach ($tasks as &$task) {
            unset($task['task']);
        }

        return $tasks ?? [];
    }

    /**
     * Mark a task as completed
     *
     * @param $task_id
     * @return array|ActiveRecord
     * @throws BadRequestHttpException
     */
    public function actionComplete($task_id)
    {
        $dailyTask = UserTasks::getCurrentDailyTask($task_id);

        if (!$dailyTask) {
            throw new BadRequestHttpException("The current task isn't in the list of your daily tasks");
        }

        $dailyTask->completed = 1;
        $dailyTask->save();

        return $dailyTask;
    }

    /**
     * Get a new task in the daily selection
     *
     * @param $task_id
     * @return array|ActiveRecord
     * @throws BadRequestHttpException
     */
    public function actionSkip($task_id)
    {
        $dailyTask = UserTasks::getCurrentDailyTask($task_id);

        if (!$dailyTask) {
            throw new BadRequestHttpException("The current task isn't in the list of your daily tasks");
        }

        $dailyTask->skip = 1;
        $dailyTask->save();

        UserTasks::createDailyTasks(Yii::$app->user->id, 1);

        return $dailyTask;
    }
}
