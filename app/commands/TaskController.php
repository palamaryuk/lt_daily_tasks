<?php

namespace app\commands;

use app\models\User;
use app\models\UserTasks;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Task Management
 *
 */
class TaskController extends Controller
{
    /**
     * Create daily tasks for all users
     *
     * @return int
     */
    public function actionCreateDailyTasks(): int
    {

        $users = User::find()->asArray()->all();

        foreach ($users as $user) {
            UserTasks::createDailyTasks($user['id']);
        }

        return ExitCode::OK;
    }
}
