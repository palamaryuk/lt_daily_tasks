<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Task;
use app\models\TaskCategories;
use Faker\Factory;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;

class FakerController extends Controller
{
    public function actionGenerateTasks()
    {
        $faker = Factory::create();

        $categories = TaskCategories::find()->asArray()->all();
        $categories = ArrayHelper::getColumn($categories, 'id');

        for ($i = 0; $i < 5000; $i++) {
            $post = new Task();
            $post->category_id = $categories[array_rand($categories, 1)];
            $post->name = $faker->text(rand(50, 100));
            $post->description = $faker->text(rand(500, 2000));
            $post->save(false);
        }

        return ExitCode::OK;
    }
}
