<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $access_token
 * @property integer $access_token_expired_at
 */

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    const ACCESS_TOKEN_LIFE_TIME = 1800;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['login', 'password'], 'required'],
            [['login'], 'string', 'max' => 32],
            [['password'], 'string', 'max' => 255],
            [['access_token'], 'string'],
            [['access_token_expired_at'], 'integer'],
            [['login'], 'unique'],
            [['login', 'password'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'access_token' => 'Access token',
            'access_token_expired_at' => 'Access token expired at',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where([
            'and',
            ['=', 'access_token', $token],
            ['>', 'access_token_expired_at', time()]
        ])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getAccessToken()
    {
        if ($this->access_token && ($this->access_token_expired_at > time())) {
            return $this->access_token;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function setAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString(32);
        $this->access_token_expired_at = self::ACCESS_TOKEN_LIFE_TIME + time();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['login' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->access_token;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        if (!$password) {
            return;
        }

        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
}
