<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "user_tasks".
 *
 * @property string|null $date_create
 * @property int $user_id
 * @property int $task_id
 * @property int|null $completed
 * @property int|null $skip
 *
 * @property Task $task
 * @property User $user
 */
class UserTasks extends \yii\db\ActiveRecord
{
    // number of tasks in the collection
    const DAILY_TASKS_COUNT = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'user_tasks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['date_create'], 'safe'],
            [['user_id', 'task_id'], 'required'],
            [['user_id', 'task_id', 'completed', 'skip'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::class, 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'date_create' => 'Date Create',
            'user_id' => 'User ID',
            'task_id' => 'Task ID',
            'completed' => 'Completed',
            'skip' => 'Skip',
        ];
    }

    public static function primaryKey(): array
    {
        return ['task_id'];
    }

    /**
     * Gets query for [[Task]].
     *
     * @return ActiveQuery
     */
    public function getTask(): ActiveQuery
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Get a list of tasks for the current day
     *
     * @param $taskID
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getCurrentDailyTask($taskID)
    {
        $query = new Query();
        $query->select('*')
            ->from(
                UserTasks::find()
                    ->select('task_id')
                    ->where([
                        'and',
                        ['=', 'user_id', Yii::$app->user->id],
                        ['=', 'skip', 0],
                        ['>=', 'date_create', gmdate('Y-m-d 00:00:00')],
                    ])
                    ->limit(self::DAILY_TASKS_COUNT)
                    ->orderBy('date_create DESC')
            );

        return UserTasks::find()
            ->where([
                'and',
                ['=', 'user_id', Yii::$app->user->id],
                ['=', 'task_id', $taskID],
                ['in', 'task_id', $query]
            ])
            ->one();
    }

    /**
     * Create a task for a user
     *
     * @param $userID
     * @param int $tasksCount
     * @throws \yii\db\Exception
     */
    public static function createDailyTasks($userID, $tasksCount = self::DAILY_TASKS_COUNT)
    {

        $dateCreate = gmdate('Y-m-d H:i:s');

        $tasks = Task::find()
            ->where([
                'not in',
                'id',
                UserTasks::find()
                    ->select('task_id')
                    ->where(['user_id' => $userID])
            ])
            ->limit($tasksCount)
            ->orderBy(new Expression('rand()'))
            ->asArray()
            ->all();

        $userTasks = [];

        foreach ($tasks as $task) {
            $userTasks[] = [
                'date_create' => $dateCreate,
                'user_id' => $userID,
                'task_id' => (int)$task['id'],
                'completed' => 0,
                'skip' => 0,
            ];
        }

        Yii::$app->db->createCommand()->batchInsert(UserTasks::tableName(), (new UserTasks())->attributes(), $userTasks)->execute();
    }
}
