<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_tasks}}`.
 */
class m220303_221209_create_user_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_tasks}}', [
            'date_create' => $this->timestamp()->defaultExpression('NOW()'),
            'user_id' => $this->integer()->notNull(),
            'task_id' => $this->integer()->notNull(),
            'completed' => $this->boolean()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-user_tasks-user_id',
            'user_tasks',
            'user_id'
        );

        $this->createIndex(
            'idx-user_tasks-task_id',
            'user_tasks',
            'task_id'
        );


        $this->addForeignKey(
            'fk-user_tasks-user_id',
            'user_tasks',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-user_tasks-task_id',
            'user_tasks',
            'task_id',
            'task',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_tasks-user_id', 'user_tasks');
        $this->dropForeignKey('fk-user_tasks-task_id', 'user_tasks');
        $this->dropIndex('idx-user_tasks-user_id', 'user_tasks');
        $this->dropIndex('idx-user_tasks-task_id', 'user_tasks');
        $this->dropTable('{{%user_tasks}}');
    }
}
