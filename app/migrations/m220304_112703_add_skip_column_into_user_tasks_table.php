<?php

use yii\db\Migration;

/**
 * Class m220304_112703_add_skip_column_into_user_tasks_table
 */
class m220304_112703_add_skip_column_into_user_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_tasks', 'skip', $this->boolean()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user_tasks', 'skip');
        echo "m220304_112703_add_skip_column_into_user_tasks_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220304_112703_add_skip_column_into_user_tasks_table cannot be reverted.\n";

        return false;
    }
    */
}
