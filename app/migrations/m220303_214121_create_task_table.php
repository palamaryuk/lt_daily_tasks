<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%task}}`.
 */
class m220303_214121_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'name' => $this->string(128)->notNull(),
            'description' => $this->text()->null(),
        ]);

        $this->createIndex(
            'idx-task-category_id',
            'task',
            'category_id'
        );

        $this->addForeignKey(
            'fk-task-category_id',
            'task',
            'category_id',
            'task_categories',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-task-category_id', 'task');
        $this->dropIndex('idx-task-category_id', 'task');
        $this->dropTable('{{%task}}');
    }
}
