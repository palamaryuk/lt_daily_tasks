<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%task_categories}}`.
 */
class m220303_211646_create_task_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%task_categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%task_categories}}');
    }
}
