<?php

use yii\db\Migration;

/**
 * Class m220303_184532_create_token_columns
 */
class m220303_184532_create_token_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'access_token', $this->string()->null());
        $this->addColumn('users', 'access_token_expired_at', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'access_token');
        $this->dropColumn('user', 'access_token_expired_at');

        echo "m220303_184532_create_token_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220303_184532_create_token_columns cannot be reverted.\n";

        return false;
    }
    */
}
