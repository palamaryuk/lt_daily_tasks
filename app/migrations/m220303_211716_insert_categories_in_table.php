<?php

use yii\db\Migration;

/**
 * Class m220303_211716_insert_categories_in_table
 */
class m220303_211716_insert_categories_in_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%task_categories}}', ['name'], [
                ['Fundamentals'],
                ['String'],
                ['Algorithms'],
                ['Mathematic'],
                ['Performance'],
                ['Booleans'],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220303_211716_insert_categories_in_table cannot be reverted.\n";

        $this->delete('{{%task_categories}}', ['in', 'name', [
            ['Fundamentals'],
            ['String'],
            ['Algorithms'],
            ['Mathematic'],
            ['Performance'],
            ['Booleans']]
        ]);

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220303_211716_insert_categories_in_table cannot be reverted.\n";

        return false;
    }
    */
}
