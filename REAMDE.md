# Запуск приложения

1. ```git clone https://gitlab.com/palamaryuk/lt_daily_tasks.git```
2. ```cd lt_daily_tasks```
3. ```docker-compose up -d```
4. ```docker exec -it lt_daily_tasks-php-1 sh```
5. ```cd /app```
6. ```composer install```
7. ```./yii migrate/up```
8. ```./yii faker/generate-tasks```

# Доступные методы

### Регистрация

```
POST only: /api/registration

json rawbody

{
    "login": "dev",
    "password" "dev"
}

response: error or user id
```

### Получение Bearer токена

```
POST only: /api/authorization

json rawbody

{
    "login": "dev",
    "password" "dev"
}

response: error or auth key
```

### Генерация ежедневных подборок.

```
./yii task/create-daily-tasks
```

### Просмотр задач на день для текущего пользователя

```
/api/task

--header 'Authorization: Bearer <token>'

response: tasks lits
```

### Отметить задачу как выполненную

```
/api/task/<task_id>/complete

--header 'Authorization: Bearer <token>'

response: completed task
```

### Заменить задачу

```
/api/task/<task_id>/skip

--header 'Authorization: Bearer <token>'

response: skiped task
```